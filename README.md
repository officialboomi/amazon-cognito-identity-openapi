# Amazon  Cognito Identity Connector
Amazon Cognito Federated Identities is a web service that delivers scoped temporary credentials to mobile devices and other untrusted environments.

Documentation: https://docs.aws.amazon.com/cognito-identity

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/cognito-identity/2014-06-30/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

